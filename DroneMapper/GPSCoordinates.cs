﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DroneMapper
{

    public class Utm
    {
        public double easting { get; set; }
        public double northing { get; set; }
        public string zone_letter { get; set; }
        public int zone_number { get; set; }
    }


    public class GPSCoordinates
    {
        public double altitude { get; set; } = 0;
        public double latitude { get; set; } = 0.0;
        public double longitude { get; set; } = 0.0;
        public Utm utm { get; set; }
        public Point AsLocationOnPicture => new Point(CountX(), CountY());



        private static readonly Point refPoint1 = new Point(164, 286);

        private static readonly GPSCoordinates refPoint1Coordinates = new GPSCoordinates()
        {
            latitude = 50.089677,
            longitude = 20.193698
        };


        private static readonly Point refPoint2 = new Point(600, 511);

        private static readonly GPSCoordinates refPoint2Coordinates = new GPSCoordinates()
        {
            latitude = 50.086558,
            longitude = 20.203074
        };

        private static readonly double ax = (refPoint1.X - refPoint2.X)/(refPoint1Coordinates.longitude - refPoint2Coordinates.longitude);

        private static readonly double bx = refPoint1.X - refPoint1Coordinates.longitude * ax;

        private static readonly double ay = (refPoint1.Y - refPoint2.Y) / (refPoint1Coordinates.latitude - refPoint2Coordinates.latitude);

        private static readonly double by =  refPoint1.Y - refPoint1Coordinates.latitude * ay;


        private int CountY()
        {
            return  Convert.ToInt32(latitude*ay + by);
        }

        private int CountX()
        {           
            return  Convert.ToInt32(longitude * ax + bx);
        }

        public string GetUtmString()
        {
            return $"{utm.zone_number} {utm.zone_letter} {utm.easting:0} E {utm.northing:0} N";
        }
    }
}