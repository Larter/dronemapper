﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DroneMapper
{
    class Graph
    {
        public Dictionary<Node, Dictionary<Node, double>> Vertices { get; } = new Dictionary<Node, Dictionary<Node, double>>();

        public void add_vertex(Node node)
        {
            Vertices[node] = new Dictionary<Node, double>();
        }

        public void add_edge(Node from, Node to)
        {
            double distance = Math.Sqrt((@from.Location.X - @from.Location.X)* (@from.Location.X - @from.Location.X) 
                                          + (@from.Location.Y - @from.Location.Y)* (@from.Location.Y - @from.Location.Y));
            Vertices[from].Add(to, distance);
            Vertices[to].Add(from,distance);
        }

        public List<Node> shortest_path(Node start, Node finish)
        {
            var previous = new Dictionary<Node, Node>();
            var distances = new Dictionary<Node, double>();
            var nodes = new List<Node>();


            List<Node> path = null;

            foreach (var vertex in Vertices)
            {
                if (vertex.Key == start)
                {
                    distances[vertex.Key] = 0;
                }
                else
                {
                    distances[vertex.Key] = double.MaxValue;
                }

                nodes.Add(vertex.Key);
            }

            while (nodes.Count != 0)
            {
                nodes.Sort((x, y) =>
                {
                    if (distances[x] > distances[y]) return 1;
                    else if (distances[x] < distances[y]) return -1;
                    else return 0;
                });

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == finish)
                {
                    path = new List<Node>();
                    while (previous.ContainsKey(smallest))
                    {
                        path.Add(smallest);
                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == double.MaxValue)
                {
                    break;
                }

                foreach (var neighbor in Vertices[smallest])
                {
                    var alt = distances[smallest] + neighbor.Value;
                    if (alt < distances[neighbor.Key])
                    {
                        distances[neighbor.Key] = alt;
                        previous[neighbor.Key] = smallest;
                    }
                }
            }
            return path;
        }

        static public Graph getFedGraph()
        {
            Graph g = new Graph();

            var drzewo = new Node(new Point(50,30), "Drzewo" );
            g.add_vertex(drzewo);

            var laka = new Node(new Point(100, 20), "Laka");
            g.add_vertex(laka);

            var pies = new Node(new Point(240, 200), "Pies");
            g.add_vertex(pies);

            var droga1 = new Node(new Point(80, 200), "Droga1");
            g.add_vertex(droga1);
            var droga2 = new Node(new Point(30, 480), "Droga2");
            g.add_vertex(droga2);
            var droga3 = new Node(new Point(300, 100), "Droga3");
            g.add_vertex(droga3);
            var droga4 = new Node(new Point(480, 180), "Droga4");
            g.add_vertex(droga4);

            g.add_edge(drzewo, laka);
            g.add_edge(laka,pies);
            g.add_edge(pies,droga4);
            g.add_edge(pies, droga2);
            g.add_edge(droga2,laka);
            g.add_edge(droga4, droga2);


            return g;
        }
    }
}
