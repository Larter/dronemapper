﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace DroneMapper
{
    public class LocationReader
    {
        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 1000;
                return w;
            }
        }
        public GPSCoordinates ReadGpsCoordinates(string url)
        {
            try
            {
                var client = new MyWebClient();
                
                var reply = client.DownloadString(url);
                if (reply == null)
                    return null;

                var coordinates = JsonConvert.DeserializeObject<GPSCoordinates>(reply);
                return coordinates;
            }
            catch(Exception)
            {
                return null;
            }
        }
    }
}
